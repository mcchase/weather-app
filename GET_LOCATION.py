import requests

def get_location():
    """ Returns the longitude and latitude for the location of this machine."""
    req = requests.request("GET", 'https://ipvigilante.com')

    location = req.json() # deserialize/parse json response to dictionary

    longitude = location["data"]['longitude']
    latitude = location["data"]['latitude']
    city = location["data"]['city_name']

    return latitude, longitude, city
