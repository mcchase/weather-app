import requests

def get_location():
    """ Returns the longitude and latitude for the location of this machine."""
    req = requests.request("GET", 'https://ipvigilante.com')

    location = req.json() # deserialize/parse json response to dictionary

    longitude = location["data"]['longitude']
    latitude = location["data"]['latitude']
    city = location["data"]['city_name']

    return latitude, longitude, city


def get_temperature(latitude, longitude, city):
    req2 = requests.request('GET', f'https://api.darksky.net/forecast/fbcf15d3d872f2f66720690d60e26343/{latitude},{longitude},{city}')
    forecast = req2.json()

    summary = forecast['currently']['summary']
    temp = forecast['currently']['temperature']
    high = forecast['daily']['data'][0]['temperatureHigh']
    low = forecast['daily']['data'][0]['temperatureLow']
    humidity = forecast['currently']['humidity']
    feelslike = forecast['currently']['apparentTemperature']


    print(summary)

    return city, summary, temp, high, low, humidity, feelslike


if __name__ == "__main__":
    longitude, latitude, city = get_location()
    city, summary, temp, high, low, humidity, feelslike = get_temperature(longitude, latitude, city)
    #print_forecast(city, summary, temp, high, low, humidity, feelslike)
