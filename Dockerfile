FROM python3.5-alpine3.8

COPY . /app

RUN pip3 install requests

ENTRYPOINT [ "python3", "/app/app.py"]